#pragma once
#ifndef FRAMETRANSFORMATIONS_H
#define FRAMETRANSFORMATIONS_H
#include <Eigen\Geometry>
namespace FrameTransformations
{
	const Eigen::AngleAxis<double> CalculateTransformationX(const double& phi);
	const Eigen::AngleAxis<double> CalculateTransformationY(const double& theta);
	const Eigen::AngleAxis<double> CalculateTransformationZ(const double& psi);
	const Eigen::Quaternion<double> CalculateTransformationXyz(const double& phi, const double& theta, const double& psi);
}
#endif // !FRAMETRANSFORMATIONS_H