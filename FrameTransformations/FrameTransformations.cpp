#include "FrameTransformations.h"

namespace FrameTransformations
{
	const Eigen::AngleAxis<double> CalculateTransformationX(const double & phi)
	{
		return Eigen::AngleAxis<double>(phi, Eigen::Vector3d::UnitX());
	}

	const Eigen::AngleAxis<double> CalculateTransformationY(const double & theta)
	{
		return Eigen::AngleAxis<double>(theta, Eigen::Vector3d::UnitY());
	}

	const Eigen::AngleAxis<double> CalculateTransformationZ(const double & psi)
	{
		return Eigen::AngleAxisd(psi, Eigen::Vector3d::UnitZ());
	}

	const Eigen::Quaternion<double> CalculateTransformationXyz(const double & phi, const double & theta, const double & psi)
	{
		return Eigen::AngleAxis<double>(phi, Eigen::Vector3d::UnitX())
			* Eigen::AngleAxis<double>(theta, Eigen::Vector3d::UnitY())
			* Eigen::AngleAxis<double>(psi, Eigen::Vector3d::UnitZ());
	}
}


