#define _USE_MATH_DEFINES

#include "FrameTransformations.h"
#include <iostream>
#include <Eigen\Geometry>
#include <math.h>

using namespace Eigen;
using namespace std;
using namespace FrameTransformations;

void TestHeading(const double& heading);
void TestPitch(const double& pitch);
void TestRoll(const double& roll);
void TestBulkhead(const double& phi, const double& theta, const double& psi);

int main()
{
	TestHeading(M_PI / 2);
	TestPitch(M_PI / 2);
	TestRoll(M_PI / 2);
	TestBulkhead(0, M_PI / 6, 0);
	system("pause");
}

void TestHeading(const double& heading)
{
	cout << "TestHeading: heading = " << heading * 180 / M_PI << "deg\n";
	const AngleAxis<double> T_h = CalculateTransformationX(heading);
	const Vector3d v1 = Vector3d(1, 0, 0);
	const Vector3d v2 = T_h * v1;
	cout << "v1 = " << endl << v1 << endl << endl;
	cout << "v2 = " << endl << v2 << endl << endl;
}

void TestPitch(const double& pitch)
{
	cout << "TestPitch: pitch = " << pitch * 180 / M_PI << "deg\n";
	const AngleAxis<double> T_p = CalculateTransformationY(pitch);
	const Vector3d v1 = Vector3d(1, 0, 0);
	const Vector3d v2 = T_p * v1;
	cout << "v1 = " << endl << v1 << endl << endl;
	cout << "v2 = " << endl << v2 << endl << endl;
}

void TestRoll(const double& roll)
{
	cout << "TestRoll: roll = " << roll * 180 / M_PI << "deg\n";
	const AngleAxis<double> T_r = CalculateTransformationZ(roll);
	const Vector3d v1 = Vector3d(0, 0, 1);
	const Vector3d v2 = T_r * v1;
	cout << "v1 = " << endl << v1 << endl << endl;
	cout << "v2 = " << endl << v2 << endl << endl;
}

void TestBulkhead(const double& phi, const double& theta, const double& psi)
{
	cout << "TestBulkhead: phi = " << phi * 180 / M_PI << "deg, theta = " << theta * 180 / M_PI << "deg, psi = " << psi * 180 / M_PI << "deg\n";
	const Quaternion<double> T_b = CalculateTransformationXyz(phi, theta, psi);
	const Vector3d v1 = Vector3d(1, 0, 0);
	const Vector3d v2 = T_b * v1;
	cout << "v1 = " << endl << v1 << endl << endl;
	cout << "v2 = " << endl << v2 << endl << endl;
}